import { extendObservable } from "mobx";
import { RootStore } from "./RootStore";
export class UserStore {
  constructor(rootStore = RootStore) {
    this.rootStore = rootStore;

    extendObservable(this, {
      user: {},

      setUser(user) {
        this.user = user;
      }
    });
  }
}
