import { createContext } from "react";
import { LeagueStore } from "./LeagueStore";
import { UserStore } from "./UserStore";
export class RootStore {
  constructor() {
    this.UserStore = new UserStore(this);
    this.LeagueStore = new LeagueStore(this);
  }
}

export const RootStoreContext = createContext(new RootStore());
