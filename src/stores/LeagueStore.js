import { extendObservable } from "mobx";
import { RootStore } from "./RootStore";
export class LeagueStore {
  constructor(rootStore = RootStore) {
    this.rootStore = rootStore;

    extendObservable(this, {
      league: "BL1",
      season: 2019,
      teams: 18,
      matchdays: 34,

      setLeague(league) {
        this.league = league;
      },
      setSeason(season) {
        this.season = parseInt(season, 10);
      },
      setTeams(teams) {
        this.teams = parseInt(teams, 10);
      },
      setMatchdays(matchdays) {
        this.matchdays = parseInt(matchdays, 10);
      },
      setComputedLeague(x) {
        const [league, season, teams, matchdays] = x.split("-");
        this.setLeague(league);
        this.setSeason(season);
        this.setTeams(teams);
        this.setMatchdays(matchdays);
      },
      getComputedLeague() {
        if (this.league && this.season && this.teams && this.matchdays) {
          return `${this.league}-${this.season}-${this.teams}-${this.matchdays}`;
        } else {
          return null;
        }
      }
    });
  }
}
