import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { useQuery } from "react-apollo-hooks";
import { overallScoreMatchdayQuery } from "../../graphql/queries/overallScoreMatchdayQuery";

const myStyles = {
  root: {
    width: "100%"
  },
  marginRight: {
    marginRight: 8
  }
};

const OverallScoreMatchday = (props) => {
  const { classes, league, season } = props;

  const {
    data: { OverallScoreMatchday },
    loading,
    error
  } = useQuery(overallScoreMatchdayQuery, {
    variables: {
      league,
      season
    }
  });

  if (error) {
    return `Error!: ${error}`;
  }

  if (loading) {
    return null;
  }

  return (
    <div
      style={{
        boxShadow: "3px 3px 5px  #666",
        marginBottom: 5,
        padding: 15
      }}
    >
      <Typography
        className={classes.marginRight}
        variant="body2"
        display="inline"
      >
        best matchday {OverallScoreMatchday.matchday}:
      </Typography>
      <Typography variant="body2" display="inline">
        {OverallScoreMatchday.name}, {OverallScoreMatchday.score} points
      </Typography>
    </div>
  );
};

export default withStyles(myStyles)(OverallScoreMatchday);
