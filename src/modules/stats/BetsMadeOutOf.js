import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { useQuery } from "react-apollo-hooks";
import { betCountLastMatchdayQuery } from "../../graphql/queries/betCountLastMatchdayQuery";

const myStyles = {
  root: {
    width: "100%"
  },
  marginRight: {
    marginRight: 8
  }
};

const BetsMadeOutOf = (props) => {
  const { classes, league, season } = props;

  const {
    data: { BetCountLastMatchday },
    loading,
    error
  } = useQuery(betCountLastMatchdayQuery, {
    variables: {
      league,
      season
    }
  });

  if (error) {
    return `Error!: ${error}`;
  }

  if (loading) {
    return null;
  }

  return (
    <div
      style={{
        boxShadow: "3px 3px 5px  #666",
        marginBottom: 5,
        padding: 15
      }}
    >
      <Typography
        className={classes.marginRight}
        variant="body2"
        display="inline"
      >
        competed matchday {BetCountLastMatchday.lastMatchday}:
      </Typography>
      <Typography variant="body2" display="inline">
        {BetCountLastMatchday.betsMade} out of {BetCountLastMatchday.usersTotal}
      </Typography>
    </div>
  );
};

export default withStyles(myStyles)(BetsMadeOutOf);
