import React from "react";
import { withApollo } from "react-apollo";
import { singleUploadMutation } from "../../graphql/mutations/uploadAvatar";

export const UploadFile = withApollo((props) => {
  const onChange = async ({
    target: {
      validity,
      files: [file]
    }
  }) => {
    if (validity["valid"]) {
      const {
        data: { uploadFile }
      } = await props.client.mutate({
        mutation: singleUploadMutation,
        variables: {
          file
        }
      });

      if (!uploadFile) {
        return;
      }
      console.log(uploadFile);
      await props.client.resetStore();
    }
  };

  return <input type="file" required onChange={onChange} />;
});
