// @ts-check
import { Field, Form, withFormik } from "formik";
import React from "react";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import * as Yup from "yup";
import { registerMutation } from "../../graphql/queries/register";
import { normalizeErrors } from "../utils/normalizeErrors";

const WrappedRegisterForm = ({ errors }) => {
  return (
    <Form
      style={{
        flex: 1,
        display: "flex",
        flexDirection: "column"
      }}
    >
      {errors.name && <div>{errors.name}</div>}
      <Field name="name" type="input" placeholder="name" />
      {errors.email && <div>{errors.email}</div>}
      <Field name="email" type="input" placeholder="email" />
      <Field
        style={{ marginBottom: 10 }}
        name="password"
        type="password"
        placeholder="password"
      />
      <button type="submit">Login</button>
    </Form>
  );
};

export const RegisterForm = withRouter(
  withApollo(
    withFormik({
      mapPropsToValues: () => ({ name: "", email: "", password: "" }),
      validationSchema: Yup.object().shape({
        name: Yup.string().required(),
        email: Yup.string()
          .email()
          .required(),
        password: Yup.string().required()
      }),
      handleSubmit: async (values, { props }) => {
        const {
          data: { register }
        } = await props.client.mutate({
          mutation: registerMutation,
          variables: values
        });

        if (register) {
          return normalizeErrors(register);
        } else {
          props.history.push("/login");
        }
      }
    })(WrappedRegisterForm)
  )
);
