import { withFormik } from "formik";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import { forgotPasswordMutation } from "../../graphql/mutations/forgotPassword";

const C = () => {
  return (
    <Form
      style={{
        flex: 1,
        display: "flex",
        flexDirection: "column"
      }}
    >
      <Field name="email" type="input" placeholder="Email" />
      <button type="submit">Reset Password</button>
    </Form>
  );
};

export const ForgotPasswordForm = withRouter(
  withApollo(
    withFormik({
      mapPropsToValues: () => ({ email: "" }),
      handleSubmit: async (values, { props }) => {
        const {
          data: { sendForgotPasswordEmail }
        } = await props.client.mutate({
          mutation: forgotPasswordMutation,
          variables: values
        });

        if (sendForgotPasswordEmail) {
          props.history.push("/m/reset-password", {
            message: "check your email to reset your password"
          });
        }
      }
    })(C)
  )
);
