// @ts-check
import { Field, Form, withFormik } from "formik";
import React from "react";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import * as Yup from "yup";
import { loginMutation } from "../../graphql/queries/login";
import { meQuery } from "../../graphql/queries/me";

const WrappedLoginForm = ({ errors }) => {
  return (
    <Form
      style={{
        flex: 1,
        display: "flex",
        flexDirection: "column"
      }}
    >
      {errors.email && <div>{errors.email}</div>}
      <Field name="email" type="input" placeholder="email" />
      <Field
        style={{ marginBottom: 10 }}
        name="password"
        type="password"
        placeholder="password"
      />
      <button type="submit">Login</button>
    </Form>
  );
};

export const LoginForm = withRouter(
  withApollo(
    withFormik({
      mapPropsToValues: () => ({ email: "", password: "" }),
      validationSchema: Yup.object().shape({
        email: Yup.string()
          .email()
          .required(),
        password: Yup.string().required()
      }),
      handleSubmit: async (values, { props, setErrors }) => {
        await props.client.resetStore();
        const {
          data: { login }
        } = await props.client.mutate({
          mutation: loginMutation,
          variables: values,
          refetchQueries: [
            {
              query: meQuery,
              variables: { me: values }
            }
          ]
        });

        if (!login || login.id === null) {
          setErrors({ email: "invalid" });
        } else {
          props.rootStore.UserStore.setUser(login);

          props.history.push("/");
        }
      }
    })(WrappedLoginForm)
  )
);
