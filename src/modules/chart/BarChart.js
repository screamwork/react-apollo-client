import { sortBy, sum } from "lodash";
import React from "react";
import { useQuery } from "react-apollo-hooks";
import { Bar } from "react-chartjs-2";
import { chartDataQuery } from "../../graphql/queries/chartData";

export const BarChart = ({ league, season, email }) => {
  const {
    data: { ChartData },
    loading,
    error
  } = useQuery(chartDataQuery, { variables: { league, season } });

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  if (ChartData && ChartData.length) {
    const start = 1;
    const end = ChartData[ChartData.length - 1].matchday + 1;
    const range = Array.from({ length: end - start }, (v, k) => k + start);

    let users = [];

    ChartData.forEach((user) => {
      let found = users.find((u) => u.email === user.email);
      if (found) {
        found.data[user.matchday - 1] = "score" in user ? user.score : 0;
      } else {
        let newUser = {};
        newUser.data = [];
        newUser.backgroundColor =
          "#" +
          "0123456789abcdef"
            .split("")
            .map(function(v, i, a) {
              return i > 5 ? null : a[Math.floor(Math.random() * 16)];
            })
            .join("");
        newUser.fill = false;
        newUser.label = user.name;
        newUser.email = user.email;
        newUser.data[user.matchday - 1] = "score" in user ? user.score : 0;
        users.push(newUser);
      }
    });

    users.map((u) => (u.totalscore = sum(u.data)));

    // sortBy -> goes ascending

    let first = sortBy(users, ["totalscore", "email"]).pop();

    let u = [];

    if (first.email === email) {
      u.push(first);
    } else {
      let filteredCurrentUser = users.find((u) => u.email === email);
      if (!filteredCurrentUser) {
        filteredCurrentUser = {};
      }
      u.push(first);
      u.push(filteredCurrentUser);
    }

    const obj = {
      labels: range,
      datasets: u
    };

    const opts = {
      responsive: true,
      maintainAspectRatio: true,
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            },
            scaleLabel: {
              display: false,
              labelString: "Score",
              fontSize: 12
            }
          }
        ],
        xAxes: [
          {
            ticks: {
              autoSkip: true,
              maxTicksLimit: 16,
              stepSize: 2
            },
            scaleLabel: {
              display: false,
              labelString: "Fixture Day",
              fontSize: 12
            }
          }
        ]
      }
    };

    return (
      <Bar
        data={obj}
        width={100}
        height={75}
        options={opts}
        datasetKeyProvider={() => Math.random()}
      />
    );
  }
};
