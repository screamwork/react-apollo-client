import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import { observer } from "mobx-react-lite";
import React from "react";
import { useQuery } from "react-apollo-hooks";
import { overallScoreQuery } from "../../graphql/queries/overallScoreQuery";
import { RootStoreContext } from "../../stores/RootStore";
import Gravatar from "./Gravatar";

const myStyles = {
  root: {
    width: "100%"
  },
  paper: {
    width: "100%",
    overflowX: "auto"
  },
  table: {
    minWidth: 650
  },
  avatar: {
    display: "flex",
    alignItems: "center"
  }
};

const OverallScore = observer((props) => {
  const rootStore = React.useContext(RootStoreContext);
  const { title, classes } = props;

  const {
    data: { OverallScore },
    loading,
    error
  } = useQuery(overallScoreQuery, {
    variables: {
      league: rootStore.LeagueStore.league,
      season: parseInt(rootStore.LeagueStore.season, 10)
    }
  });

  if (loading) {
    return null;
  }
  if (error) {
    return `Error!: ${error}`;
  }

  return (
    <div
      style={{
        boxShadow: "3px 3px 5px  #666",
        marginBottom: 5,
        padding: 15
      }}
    >
      {/* <h4 style={{ margin: "0 0 5px" }}>{title}</h4> */}
      <Typography variant="h6">{title}</Typography>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Table
            className={classes.table}
            size="small"
            aria-label="a dense table"
          >
            <TableHead>
              <TableRow>
                <TableCell>Player</TableCell>
                <TableCell align="right">bets</TableCell>
                <TableCell align="right">Points</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {OverallScore.map((item, idx) => {
                // console.log(item);
                return (
                  <TableRow key={idx}>
                    <TableCell align="left">
                      <div className={classes.avatar}>
                        <Gravatar
                          size={30}
                          email={item.email}
                          avatar={item.avatar}
                        />
                        <Typography display="inline">{item.name}</Typography>
                      </div>
                    </TableCell>
                    <TableCell align="right">
                      <Typography>{item.betdays}</Typography>
                    </TableCell>
                    <TableCell align="right">
                      <Typography>{item.score}</Typography>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>
      </div>
    </div>
  );
});

export default withStyles(myStyles)(OverallScore);
