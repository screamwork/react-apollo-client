import { withStyles } from "@material-ui/core/styles";
import gravatar from "gravatar";
import React from "react";

const myStyles = {
  img: {
    backgroundSize: "cover",
    marginRight: 10
  }
};

const Gravatar = (props) => {
  const { size, email, avatar, classes } = props;
  let gravatarUrl = gravatar.url(email, { s: 20 }, true);
  if (avatar && avatar !== "") {
    gravatarUrl = process.env.REACT_APP_APOLLO_SERVER + avatar;
  } else {
    if (gravatarUrl.includes("404")) {
      gravatarUrl =
        "https://s.gravatar.com/avatar/3b836709a12945f5868fc89d92a6418c?s=20";
    }
  }

  return (
    <img
      src={gravatarUrl}
      alt="gravatar"
      height={size}
      width={size}
      className={classes.img}
    />
  );
};

export default withStyles(myStyles)(Gravatar);
