import { Field, Form, withFormik } from "formik";
import React from "react";
import { withApollo } from "react-apollo";
import { withRouter } from "react-router";
import * as Yup from "yup";
import { forgotPasswordChangeMutation } from "../../graphql/mutations/forgotPasswordChange";

class C extends React.PureComponent {
  render() {
    return (
      <Form
        style={{
          flex: 1,
          display: "flex",
          flexDirection: "column"
        }}
      >
        <Field name="newPassword" type="password" placeholder="New Password" />
        <button type="submit">Change Password</button>
      </Form>
    );
  }
}

export const ChangePasswordForm = withRouter(
  withApollo(
    withFormik({
      validationSchema: Yup.object().shape({
        newPassword: Yup.string()
          .min(3, "passwordNotLongEnough")
          .max(255)
          .required()
      }),
      mapPropsToValues: () => ({ newPassword: "" }),
      handleSubmit: async (values, { props, setErrors }) => {
        const { key } = props.match.params;
        const {
          data: { forgotPasswordChange }
        } = await props.client.mutate({
          mutation: forgotPasswordChangeMutation,
          variables: { ...values, key }
        });

        console.log(forgotPasswordChange);

        if (forgotPasswordChange) {
          setErrors(forgotPasswordChange);
        } else {
          props.history.push("/login");
        }
      }
    })(C)
  )
);
