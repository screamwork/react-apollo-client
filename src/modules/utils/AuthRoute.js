import React from "react";
import { graphql } from "react-apollo";
import { Redirect, Route } from "react-router-dom";
import { meQuery } from "../../graphql/queries/me";
class C extends React.PureComponent {
  renderRoute = (routeProps) => {
    const { data, component: Component } = this.props;

    if (!data || data.loading) {
      // loading screen
      return null;
    }

    if (!data.me) {
      // user not logged in
      return <Redirect to="/login" />;
    }

    return <Component {...routeProps} />;
  };

  render() {
    const { data: _, component: __, ...rest } = this.props;
    return <Route {...rest} render={this.renderRoute} />;
  }
}

export default graphql(meQuery)(C);
