export const normalizeErrors = (errors) => {
  const errMap = {};

  errors.forEach((err) => {
    errMap[err.path] = err.message;
  });

  return errMap;
};
