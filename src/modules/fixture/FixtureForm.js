import { omit } from "lodash";
import moment from "moment-timezone";
import React, { useEffect, useState } from "react";
import { Mutation } from "react-apollo";
import { createBet } from "../../graphql/queries/createBet";
import { userBet } from "../../graphql/queries/userBet";
import { AfterFixtureForm } from "./AfterFixtureForm";
import { FixtureDaySelect } from "./FixtureDaySelect";
import { InnerForm } from "./InnerForm";

export const FixtureForm = (props) => {
  const [mode, setMode] = useState("result");
  const [showCheck, setShowCheck] = useState(false);
  const [bets, setBets] = useState({});
  const [fixtures, setFixtures] = useState({});

  useEffect(() => {
    prepareFixtureData(props.fixtures);
    prepareBetData(props.bets);
  }, [props.bets, props.fixtures]);

  const prepareBetData = (data) => {
    if (data) {
      let bets = {};
      for (let val of data) {
        bets[val.game] = val;
      }
      setBets(bets);
    }
  };

  const prepareFixtureData = (data) => {
    if (data) {
      let fixtures = {};
      for (let val of data) {
        fixtures[val.game] = val;
      }
      setFixtures(fixtures);
    }
  };

  const onChildStateChange = (e) => {
    let gameid = e.currentTarget.dataset.gameid;
    let mergedBets = Object.assign(
      {},
      bets,
      (bets[gameid][e.target.name] = e.target.value
        ? parseInt(e.target.value, 10)
        : "")
    );
    setBets(mergedBets);
  };

  const onChangeMode = (x) => {
    setMode(x);
  };

  const toggleCheck = () => {
    setShowCheck(true);

    setTimeout(() => {
      setShowCheck(false);
    }, 1000);
  };

  return (
    <Mutation
      update={async (cache, { data: { CreateBet } }) => {
        await cache.writeQuery({
          query: userBet,
          variables: {
            matchday: props.activeMatchday,
            league: props.league,
            season: props.season
          },
          data: {
            UserBet: CreateBet
          }
        });
      }}
      mutation={createBet}
    >
      {(mutate) => (
        <div style={{ padding: "5px 10px" }}>
          <form
            onSubmit={async (e) => {
              e.preventDefault();
              let omitted = [];
              Object.values(bets).forEach((game) => {
                if (game.betGoalAway === "" || game.betGoalHome === "") {
                  return;
                }
                game = omit(game, "__typename");
                game["gameDate"] = moment(parseInt(game.gameDate, 10))
                  .tz("Europe/Berlin")
                  .format("DD MMMM YYYY HH:mm");
                omitted.push(game);
              });
              await mutate({
                variables: { input: omitted }
              });

              toggleCheck();
            }}
          >
            <FixtureDaySelect
              activeMatchday={props.activeMatchday}
              matchdays={props.matchdays}
              onMatchdayChange={props.onMatchdayChange}
            />

            <InnerForm
              mode={mode}
              fixtures={fixtures}
              bets={bets}
              onStateChange={onChildStateChange}
            />

            <AfterFixtureForm
              mode={mode}
              onChangeMode={onChangeMode}
              showCheck={showCheck}
            />
          </form>
        </div>
      )}
    </Mutation>
  );
};
