import moment from "moment-timezone";
import React from "react";

export const InnerForm = ({ fixtures, bets, mode, onStateChange }) => {
  const checkIfInputIsDisabled = (gameDate) => {
    return moment().diff(moment(parseInt(gameDate, 10))) > 0;
  };

  return Object.values(fixtures).map((fixture) => {
    return (
      <div key={fixture.game}>
        {/* gather matches with same gamedate */}
        {(fixture.game === 1 ||
          fixtures[fixture.game].gameDate !==
            fixtures[fixture.game - 1].gameDate) && (
          <div
            style={{
              padding: "15px 0 0",
              textAlign: "center",
              fontSize: "12px",
              fontWeight: "bold"
            }}
          >
            {moment(parseInt(fixture.gameDate, 10))
              .tz("Europe/Berlin")
              .format("DD MMMM YYYY HH:mm")}
          </div>
        )}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            padding: "10px 0"
          }}
        >
          <div
            style={{
              width: "calc((100% - 60px) / 2)",
              display: "flex",
              justifyContent: "flex-end",
              alignItems: "center"
            }}
          >
            <span style={{ textAlign: "right" }}>{fixture.home.shortName}</span>
            <img
              style={{ width: 40, maxHeight: 40, padding: "0 5px" }}
              src={`${process.env.REACT_APP_APOLLO_SERVER}${fixture.home.logo}`}
              alt="home-team-logo"
            />
          </div>
          <div style={{ flex: "0 0 120px", textAlign: "center" }}>
            {mode === "result" ? (
              <input
                style={{ width: 40, height: 40, textAlign: "center" }}
                value={fixture.result ? fixture.result.goalHome : "-"}
                readOnly
              />
            ) : (
              <input
                style={{ width: 40, height: 40, textAlign: "center" }}
                value={
                  bets[fixture.game].betGoalHome !== 100
                    ? bets[fixture.game].betGoalHome
                    : ""
                }
                name="betGoalHome"
                data-gameid={fixture.game}
                onChange={(e) => onStateChange(e)}
                disabled={checkIfInputIsDisabled(bets[fixture.game].gameDate)}
              />
            )}
            <span style={{ padding: "0 10px" }}>:</span>
            {mode === "result" ? (
              <input
                style={{ width: 40, height: 40, textAlign: "center" }}
                value={fixture.result ? fixture.result.goalAway : "-"}
                readOnly
              />
            ) : (
              <input
                style={{ width: 40, height: 40, textAlign: "center" }}
                value={
                  bets[fixture.game].betGoalAway !== 100
                    ? bets[fixture.game].betGoalAway
                    : ""
                }
                name="betGoalAway"
                data-gameid={fixture.game}
                onChange={(e) => onStateChange(e)}
                disabled={checkIfInputIsDisabled(bets[fixture.game].gameDate)}
              />
            )}
          </div>
          <div
            style={{
              width: "calc((100% - 60px) / 2",
              display: "flex",
              alignItems: "center"
            }}
          >
            <img
              style={{ width: 40, maxHeight: 40, padding: "0 5px" }}
              src={`${process.env.REACT_APP_APOLLO_SERVER}${fixture.away.logo}`}
              alt="away-team-logo"
            />
            <span>{fixture.away.shortName}</span>
          </div>
        </div>
      </div>
    );
  });
};
