import { Button } from "@material-ui/core";
import React from "react";
import { FaCheck } from "react-icons/fa";

export const AfterFixtureForm = ({ onChangeMode, showCheck, mode }) => {
  return (
    <div
      style={{
        padding: "5px 10px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center"
      }}
    >
      <div className="modes">
        <Button
          style={{ marginRight: 16 }}
          variant="outlined"
          color="primary"
          onClick={() => onChangeMode("result")}
        >
          Result
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => onChangeMode("bet")}
        >
          Bet
        </Button>
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          minWidth: "100px",
          padding: "15px 0"
        }}
      >
        <div>{showCheck && <FaCheck />}</div>
        <Button type="submit" variant="outlined" color="secondary">
          Save
        </Button>
      </div>
    </div>
  );
};
