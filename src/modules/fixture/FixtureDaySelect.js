import React from "react";

export const FixtureDaySelect = ({
  activeMatchday,
  onMatchdayChange,
  matchdays
}) => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center"
      }}
    >
      <h4 style={{ margin: "5px" }}>Fixture Day</h4>
      <select
        value={activeMatchday === 999 ? 1 : activeMatchday}
        style={{ width: "25%", height: "40px" }}
        onChange={(e) => onMatchdayChange(e.target.value)}
      >
        {matchdays.map((day) => (
          <option value={day} key={day}>
            {day}
          </option>
        ))}
      </select>
    </div>
  );
};
