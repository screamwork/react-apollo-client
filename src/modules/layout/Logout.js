import { Button } from "@material-ui/core";
import React, { useContext } from "react";
import { Mutation } from "react-apollo";
import { withRouter } from "react-router";
import { logoutMutation } from "../../graphql/queries/logout";
import { meQuery } from "../../graphql/queries/me";
import { RootStoreContext } from "../../stores/RootStore";

const WrappedLogoutLink = (props) => {
  const rootStore = useContext(RootStoreContext);

  return (
    <Mutation
      update={(cache, { data }) => {
        if (!data || !data.logout) {
          return;
        }
        let q = cache.readQuery({ query: meQuery });
        q.name = q.email = q.id = null;
        cache.writeQuery({
          query: meQuery,
          data: q
        });
      }}
      mutation={logoutMutation}
    >
      {(mutate, { client }) => (
        <Button
          variant="outlined"
          color="secondary"
          onClick={async () => {
            await mutate({}).then(() => client.resetStore());
            rootStore.UserStore.setUser({});
            props.history.push("/");
          }}
        >
          Logout
        </Button>
      )}
    </Mutation>
  );
};

export const LogoutLink = withRouter(WrappedLogoutLink);
