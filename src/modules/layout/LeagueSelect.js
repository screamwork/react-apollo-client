import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { useQuery } from "react-apollo-hooks";
import { identifier } from "../../graphql/queries/identifier";
import { RootStoreContext } from "../../stores/RootStore";

export const LeagueSelect = observer(() => {
  const rootStore = useContext(RootStoreContext);
  const {
    data: { AllIdentifier },
    loading,
    error
  } = useQuery(identifier);

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;

  return (
    <select
      value={rootStore.LeagueStore.getComputedLeague() || ""}
      style={{ width: "100%", height: "40px" }}
      onChange={(e) => {
        rootStore.LeagueStore.setComputedLeague(e.target.value);
      }}
    >
      <option value="">select a league...</option>
      {AllIdentifier.map((l, index) => (
        <option
          value={`${l.league}-${l.season}-${l.teams}-${l.matchdays}`}
          key={index}
        >
          {l.name} [ {l.season} / {l.season + 1} ]
        </option>
      ))}
    </select>
  );
});

export default LeagueSelect;
