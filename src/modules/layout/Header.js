import { AppBar, Fab, Toolbar } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { CalendarViewDayOutlined, PersonPin } from "@material-ui/icons";
import { omit } from "lodash";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { Query } from "react-apollo";
import { Link } from "react-router-dom";
import { meQuery } from "../../graphql/queries/me";
import { RootStoreContext } from "../../stores/RootStore";
import { LeagueSelect } from "./LeagueSelect";

const styles = {
  root: {
    flexGrow: 1,
    paddingBottom: 16
  },
  title: {
    flexGrow: 1
  },
  extendedIcon: {
    marginRight: 8
  },
  marginRight: {
    marginRight: 8
  },
  logo: {
    textDecoration: "none",
    color: "white"
  },
  leagueSelect: {
    marginTop: 80
  }
};

const Header = observer((props) => {
  const rootStore = useContext(RootStoreContext);
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <Link to="/" className={classes.logo}>
              rpl2018
            </Link>
          </Typography>
          <Query query={meQuery}>
            {({ data, loading, error }) => {
              if (error) {
                return <pre>{error.message}</pre>;
              }
              if (loading) {
                return null;
              }
              if (!loading && !data.me) {
                return (
                  <div>
                    <Link to="/login">
                      <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        aria-label="add"
                        className={classes.extendedIcon}
                      >
                        Login
                      </Fab>
                    </Link>
                    <Link to="/register">
                      <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        aria-label="add"
                      >
                        Register
                      </Fab>
                    </Link>
                  </div>
                );
              }
              if (data.me) {
                rootStore.UserStore.setUser(omit(data.me, ["__typename"]));

                return (
                  <div>
                    <Link to="/fixture">
                      <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        aria-label="add"
                        className={classes.extendedIcon}
                      >
                        <CalendarViewDayOutlined
                          className={classes.marginRight}
                        />
                        Fixtures
                      </Fab>
                    </Link>

                    <Link to="/account">
                      <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        aria-label="add"
                      >
                        <PersonPin className={classes.marginRight} />
                        {data.me.name}
                      </Fab>
                    </Link>
                  </div>
                );
              }
            }}
          </Query>
        </Toolbar>
      </AppBar>
      <div className={classes.leagueSelect}>
        <LeagueSelect />
      </div>
    </div>
  );
});

export default withStyles(styles)(Header);
