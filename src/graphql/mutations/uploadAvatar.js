import gql from "graphql-tag";

export const singleUploadMutation = gql`
  mutation UploadFile($file: Upload!) {
    uploadFile(file: $file) {
      filename
      mimetype
    }
  }
`;
