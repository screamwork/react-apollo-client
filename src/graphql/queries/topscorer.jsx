import gql from 'graphql-tag';

export const topscorer = gql`
  query Topscorer($league: String!, $season: Int!) {
    Topscorer(league: $league, season: $season) {
      name
      teamName
      goals
      team {
        logo
      }
    }
  }
`;
