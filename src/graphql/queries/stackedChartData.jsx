import gql from 'graphql-tag';

export const stackedChartDataQuery = gql`
  query StackedChartData($league: String!, $season: Int!) {
    StackedChartData(league: $league, season: $season) {
      name
      score
      matchday
    }
  }
`;
