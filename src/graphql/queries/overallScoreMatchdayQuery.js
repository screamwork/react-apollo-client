import gql from "graphql-tag";

export const overallScoreMatchdayQuery = gql`
  query OverallScoreMatchday($league: String!, $season: Int!) {
    OverallScoreMatchday(league: $league, season: $season) {
      name
      score
      betdays
      matchday
    }
  }
`;
