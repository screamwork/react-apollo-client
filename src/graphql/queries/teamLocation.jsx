import gql from 'graphql-tag';

export const teamLocation = gql`
  query TeamLocation($league: String!, $season: Int!) {
    TeamLocation(league: $league, season: $season) {
      teamName
      lat
      lng
      venue
      logo
    }
  }
`;
