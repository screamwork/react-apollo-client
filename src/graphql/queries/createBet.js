import gql from "graphql-tag";

export const createBet = gql`
  mutation CreateBet($input: [UserBet]) {
    CreateBet(input: $input) {
      ident
      league
      season
      matchday
      game
      gameDate
      betGoalHome
      betGoalAway
    }
  }
`;
