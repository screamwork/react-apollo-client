import gql from "graphql-tag";

export const chartDataQuery = gql`
  query ChartData($league: String!, $season: Int!) {
    ChartData(league: $league, season: $season) {
      name
      email
      score
      matchday
    }
  }
`;
