import gql from "graphql-tag";

export const registerMutation = gql`
  mutation RegisterMutation(
    $name: String!
    $email: String!
    $password: String!
  ) {
    register(name: $name, email: $email, password: $password) {
      path
      message
    }
  }
`;
