import gql from "graphql-tag";

export const fixturesAndBets = gql`
  query FixturesAndBets($matchday: Int!, $league: String!, $season: Int!) {
    FixturesAndBets(matchday: $matchday, league: $league, season: $season) {
      league
      season
      fixtures {
        ident
        matchday
        game
        gameDate
        home {
          teamName
          shortName
          logo
        }
        away {
          teamName
          shortName
          logo
        }
        result {
          goalHome
          goalAway
        }
      }
    }
    UserBet(league: $league, season: $season, matchday: $matchday) {
      ident
      league
      season
      matchday
      game
      gameDate
      betGoalHome
      betGoalAway
    }
  }
`;
