import gql from "graphql-tag";

export const identFixtureByMatchday = gql`
  query IdentFixtureByMatchday(
    $matchday: Int!
    $league: String!
    $season: Int!
  ) {
    IdentFixtureByMatchday(
      matchday: $matchday
      league: $league
      season: $season
    ) {
      league
      season
      fixtures {
        ident
        matchday
        game
        gameDate
        home {
          teamName
          shortName
          logo
        }
        away {
          teamName
          shortName
          logo
        }
        result {
          goalHome
          goalAway
        }
      }
    }
  }
`;
