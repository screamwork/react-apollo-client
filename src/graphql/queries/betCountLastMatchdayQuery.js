import gql from "graphql-tag";

export const betCountLastMatchdayQuery = gql`
  query BetCountLastMatchday($league: String!, $season: Int!) {
    BetCountLastMatchday(league: $league, season: $season) {
      betsMade
      usersTotal
      lastMatchday
    }
  }
`;
