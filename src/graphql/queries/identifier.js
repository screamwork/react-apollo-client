import gql from "graphql-tag";

export const identifier = gql`
  query AllIdentifier {
    AllIdentifier {
      league
      season
      name
      teams
      matchdays
    }
  }
`;
