import gql from 'graphql-tag';

export const getLastMatchday = gql`
  query GetLastMatchday($league: String!, $season: String!) {
    LastMatchday(league: $league, season: $season)
  }
`;
