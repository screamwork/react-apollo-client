import gql from "graphql-tag";

export const fixtureByMatchday = gql`
  query FixtureByMatchday($matchday: Int!, $userId: String!) {
    FixtureByMatchday(matchday: $matchday, userId: $userId) {
      ident
      matchday
      game
      gameDate
      home {
        teamName
        shortName
        logo
      }
      away {
        teamName
        shortName
        logo
      }
      bet {
        betGoalHome
        betGoalAway
      }
      result {
        goalHome
        goalAway
      }
    }
  }
`;
