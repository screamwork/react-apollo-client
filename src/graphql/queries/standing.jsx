import gql from 'graphql-tag';

export const standing = gql`
  query Standing($league: String!, $season: Int!) {
    Standing(league: $league, season: $season) {
      position
      teamName
      played
      won
      draw
      lost
      points
      goalsFor
      goalsAgainst
      goalDiff
      logo
    }
  }
`;
