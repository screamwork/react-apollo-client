import gql from "graphql-tag";

export const overallScoreQuery = gql`
  query OverallScoreQuery($league: String!, $season: Int!) {
    OverallScore(league: $league, season: $season) {
      name
      email
      avatar
      score
      betdays
    }
  }
`;
