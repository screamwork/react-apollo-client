import gql from "graphql-tag";

export const userBet = gql`
  query UserBet($league: String!, $season: Int!, $matchday: Int!) {
    UserBet(league: $league, season: $season, matchday: $matchday) {
      ident
      league
      season
      matchday
      game
      gameDate
      betGoalHome
      betGoalAway
    }
  }
`;
