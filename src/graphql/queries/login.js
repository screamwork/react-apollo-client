import gql from "graphql-tag";
import { userFragment } from "../fragments/userFragment";

export const loginMutation = gql`
  mutation LoginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ...UserInfo
    }
  }

  ${userFragment}
`;
