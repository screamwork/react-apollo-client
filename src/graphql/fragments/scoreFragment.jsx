import gql from 'graphql-tag';

export const scoreFragment = gql`
  fragment ScoreInfo on Score {
    name
    score
  }
`;
