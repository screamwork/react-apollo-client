import React, { useContext } from "react";
import { RegisterForm } from "../modules/register/RegisterForm";
import { RootStoreContext } from "../stores/RootStore";

export const RegisterView = () => {
  const rootStore = useContext(RootStoreContext);
  return <RegisterForm rootStore={rootStore} />;
};
