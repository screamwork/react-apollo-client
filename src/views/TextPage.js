import React from "react";

export class TextPage extends React.PureComponent {
  render() {
    const {
      location: { state }
    } = this.props;
    return <h2>{state && state.message ? state.message : "hello"}</h2>;
  }
}
