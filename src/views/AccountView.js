import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { BarChart } from "../modules/chart/BarChart";
import { LogoutLink } from "../modules/layout/Logout";
import { UploadFile } from "../modules/upload/UploadAvatar";
import { RootStoreContext } from "../stores/RootStore";

const myStyles = {
  padding: {
    padding: 16
  },
  section: {
    margin: "16px 0"
  },
  flexCenter: {
    display: "flex",
    alignItems: "center"
  }
};

const AccountView = observer((props) => {
  const rootStore = useContext(RootStoreContext);
  const { classes } = props;
  return (
    <div style={{ backgroundColor: "#e9e9e9" }}>
      <Grid container>
        <Grid item sm={12} md={6}>
          <div className={classes.padding}>
            <BarChart
              league={rootStore.LeagueStore.league}
              season={parseInt(rootStore.LeagueStore.season, 10)}
              email={rootStore.UserStore.user.email}
            />
          </div>
        </Grid>
        <Grid item sm={12} md={6}>
          <div className={classes.padding}>
            <div className={classes.section}>
              <pre>{JSON.stringify(rootStore.UserStore.user, null, 2)}</pre>
            </div>

            <div className={(classes.section, classes.flexCenter)}>
              <span
                style={{
                  backgroundImage: `url(
                    ${process.env.REACT_APP_APOLLO_SERVER}${rootStore.UserStore.user.avatar}
                  )`,
                  display: "inline-block",
                  width: 35,
                  height: 35,
                  marginRight: 15,
                  borderRadius: "50%",
                  backgroundSize: "cover"
                }}
              ></span>
              <UploadFile />
            </div>

            <div className={classes.section}>
              <LogoutLink />
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
});

export default withStyles(myStyles)(AccountView);
