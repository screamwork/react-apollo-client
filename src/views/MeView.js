import React from "react";
import { Query } from "react-apollo";
import { meQuery } from "../graphql/queries/me";

export const MeView = () => {
  return (
    <Query query={meQuery}>
      {({ data, loading, error }) => {
        if (loading) {
          return null;
        }
        if (error) {
          return <div>{JSON.stringify(error)}</div>;
        }
        if (data || data.me) {
          return <div>{JSON.stringify(data)}</div>;
        }
      }}
    </Query>
  );
};
