// @ts-check
import React, { useContext } from "react";
import { LoginForm } from "../modules/login/LoginForm";
import { RootStoreContext } from "../stores/RootStore";

export const LoginView = () => {
  const rootStore = useContext(RootStoreContext);
  return <LoginForm rootStore={rootStore} />;
};
