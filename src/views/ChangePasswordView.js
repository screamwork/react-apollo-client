// @ts-check
import React, { useContext } from "react";
import { ChangePasswordForm } from "../modules/change-password/ChangePasswordForm";
import { RootStoreContext } from "../stores/RootStore";

export const ChangePasswordView = () => {
  const rootStore = useContext(RootStoreContext);
  return <ChangePasswordForm rootStore={rootStore} />;
};
