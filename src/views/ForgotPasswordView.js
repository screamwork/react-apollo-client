import React, { useContext } from "react";
import { ForgotPasswordForm } from "../modules/forgot-password/ForgotPasswordForm";
import { RootStoreContext } from "../stores/RootStore";

export const ForgotPasswordView = () => {
  const rootStore = useContext(RootStoreContext);
  console.log(rootStore);
  return <ForgotPasswordForm />;
};
