import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import OverallScore from "../modules/score/OverallScore";
import BetsMadeOutOf from "../modules/stats/BetsMadeOutOf";
import OverallScoreMatchday from "../modules/stats/OverallScoreMatchday";
import { RootStoreContext } from "../stores/RootStore";

const myStyles = {
  root: {
    margin: "16px 0"
  },
  img: {
    backgroundSize: "cover",
    marginRight: 10
  }
};

const HomeView = observer((props) => {
  const rootStore = useContext(RootStoreContext);
  const { classes } = props;

  if (rootStore.LeagueStore.league && rootStore.LeagueStore.season) {
    return (
      <div style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ flex: 1 }}>
          <Grid container className={classes.root}>
            <Grid item sm={6}>
              <BetsMadeOutOf
                league={rootStore.LeagueStore.league}
                season={rootStore.LeagueStore.season}
              />
            </Grid>
            <Grid item sm={6}>
              <OverallScoreMatchday
                league={rootStore.LeagueStore.league}
                season={rootStore.LeagueStore.season}
              />
            </Grid>
          </Grid>
          <OverallScore title="Overall Score" />
        </div>
      </div>
    );
  }
});

export default withStyles(myStyles)(HomeView);
