import { observer } from "mobx-react-lite";
import React, { useContext, useState } from "react";
import { useQuery } from "react-apollo-hooks";
import { identFixtureByMatchday } from "../graphql/queries/identFixtureByMatchday";
import { userBet } from "../graphql/queries/userBet";
import { FixtureForm } from "../modules/fixture/FixtureForm";
import { RootStoreContext } from "../stores/RootStore";

export const FixtureView = observer(() => {
  const rootStore = useContext(RootStoreContext);
  const [activeMatchday, setActiveMatchday] = useState(999);

  const initMatchdays = () => {
    const start = 1;
    const end = parseInt(rootStore.LeagueStore.matchdays, 10);
    const range = Array.from({ length: end }, (v, k) => k + start);
    return range;
  };

  const onMatchdayChange = (value) => {
    setActiveMatchday(parseInt(value, 10));
  };

  const {
    data: { UserBet }
  } = useQuery(userBet, {
    variables: {
      matchday: activeMatchday,
      league: rootStore.LeagueStore.league,
      season: parseInt(rootStore.LeagueStore.season, 10)
    }
  });

  const {
    data: { IdentFixtureByMatchday }
  } = useQuery(identFixtureByMatchday, {
    variables: {
      matchday: activeMatchday,
      league: rootStore.LeagueStore.league,
      season: parseInt(rootStore.LeagueStore.season, 10)
    }
  });

  if (!UserBet || !IdentFixtureByMatchday) {
    return <div>load...</div>;
  }

  return (
    <div className="fixture-container">
      <FixtureForm
        fixtures={IdentFixtureByMatchday.fixtures}
        bets={UserBet}
        matchdays={initMatchdays()}
        activeMatchday={
          activeMatchday === 999
            ? IdentFixtureByMatchday.fixtures[0].matchday
            : activeMatchday
        }
        league={rootStore.LeagueStore.league}
        season={rootStore.LeagueStore.season}
        onMatchdayChange={(e) => onMatchdayChange(e)}
      />
    </div>
  );
});
