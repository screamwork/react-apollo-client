// @ts-check
import { Container, CssBaseline } from "@material-ui/core";
import {
  createMuiTheme,
  MuiThemeProvider,
  responsiveFontSizes
} from "@material-ui/core/styles";
import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./modules/layout/Header";
import AccountView from "./views/AccountView";
import { ChangePasswordView } from "./views/ChangePasswordView";
import { FixtureView } from "./views/FixtureView";
import { ForgotPasswordView } from "./views/ForgotPasswordView";
import HomeView from "./views/HomeView";
import { LoginView } from "./views/LoginView";
import { RegisterView } from "./views/RegisterView";
import { TextPage } from "./views/TextPage";

let theme = createMuiTheme({
  overrides: {}
});
theme = responsiveFontSizes(theme);
console.log(theme);

const Routes = () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Container>
      <BrowserRouter>
        <Switch>
          <Route
            path="/"
            render={() => (
              <>
                <Header />
                <Route exact={true} path="/" component={HomeView} />
                <Route path="/login" component={LoginView} />
                <Route path="/register" component={RegisterView} />
                <Route path="/account" component={AccountView} />
                <Route path="/fixture" component={FixtureView} />
                <Route path="/forgot-password" component={ForgotPasswordView} />
                <Route
                  path="/change-password/:key"
                  component={ChangePasswordView}
                />
                <Route path="/m" component={TextPage} />
              </>
            )}
          />
        </Switch>
      </BrowserRouter>
    </Container>
  </MuiThemeProvider>
);

export default Routes;
