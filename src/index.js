// @ts-check
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { createUploadLink } from "apollo-upload-client";
import React from "react";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHookProvider } from "react-apollo-hooks";
import ReactDOM from "react-dom";
import Routes from "./Routes";
import * as serviceWorker from "./serviceWorker";
require("dotenv").config();

console.log(".env ", process.env);

const cache = new InMemoryCache();

const client = new ApolloClient({
  link: createUploadLink({
    // uri: 'https://graphql-server-football-data.herokuapp.com/graphql',
    uri: "http://localhost:4000",
    credentials: "include"
  }),
  cache
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <ApolloHookProvider client={client}>
      <Routes />
    </ApolloHookProvider>
  </ApolloProvider>,
  document.getElementById("root")
);

serviceWorker.unregister();
